# CSE253 Project: Protecting RIP against dishonest routers

By: Spencer Neuschmid

The description and analysis of this project can be found in this repository under the name: **CSE253_Project_Spencer_Neuschmid.pdf**

This README gives a basic overview of what each file does and how to compile the project.


## File Descriptions

**rip.cc/h**: This is the main RIP router code implementation. A majority of the features in RIP have been implemented, but some features have been left out like application traffic forwarding. All of the routing functionality is there.

**rip_header.cc/h**: These files implement RIP routing table entries and the signaling messages used by RIP routers.

**crypto_lib.cc/h**: This file implements the RSA signing and verification process using OpenSSL's implementation of RSA. Code was borrowed and modified from the example code at: https://gist.github.com/irbull/08339ddcd5686f509e9826964b17bb59

**sockets_lib.cc/h**: These files implement the socket programming needed for simulated routers. They also do all of the serialization of the RIP messages using the helper library cereal: http://uscilab.github.io/cereal/quickstart.html

**cereal/**: Cereal header library that was imported to take care of basic serialization of data. 

**router.cc**: This file is the main code that runs each of the RIP routers. There is test code for each of the individual features required to get RIP to work, but they are disabled.

**cli_test.cc**: This file includes test code that was used to get the socket and serialization code to working point where it could be implemented with a RIP router.

**router_info.h**: This file sets up much of the simulation including each of the neighbor routers, starting routes, and public/private keys.

**Makefile**: Used for code compilation. Please see next section to learn how to compile and run the code.

---

## Compile and Run the Project

To compile the project, simply run:

```
make
```

This will create two executable files: *router* and *test*

**router** is the RIP router code that was described in the previous section from **router.cc**

**test** is test code used to test sockets and serialization. It will not currently run properly because the **router.cc** code does not have the corresponding test code enabled. This code runs from the **cli_test.cc** file explained above.

The main focus is the *router* program. To run this program, run the following command:

```
./router -i <router_id>
```

The *<router_id>* specifies which router in the topology you want to run. Consult the simulation setup file to understand what routers can and can't be run. Consult the project report mentioned above to understand exactly how the topology is set up with diagrams.

To run the default simulation, run the following 6 commands: 
```
./router -i 1
./router -i 2
./router -i 3
./router -i 4 
./router -i 5
./router -i 6
```
This will begin the default simulation. The text printed by each router is its routing table after each RIP header message received from its neighbors. You should start to see routing tables be updated as the simulation continues. When convergence, or steady state, is reached, then you should see that each router has a routing table with 7 entries. Each entry explains how to get to each other router in the network.  Router 5 is an adversary in this simulation and tries to advertise a faulty route to the 10.0.3.0/24 network. Make sure that no router believed it. This confirms that the implementation with metric signing was successful. Further results and analysis can be found in the project report.

Logging can be enabled by turning on a macro in the **rip.cc** file that will allow for additional debug information so the entire RIP process can be oberved. 

If you have any questions about this implementation, please email the auther Spencer Neuschmid at sneuschm@ucsc.edu.

---