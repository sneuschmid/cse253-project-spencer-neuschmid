

#include "rip-header.h"
#include "rip.h"

#define LOG (0)


void node_info::init(char addr[16], uint16_t port, uint16_t id)
{
	// Initialize a neighbor node info.
	memcpy(n_address, addr, 16);
	n_port = port;
	n_id = id;
}

uint16_t node_info::GetPort(void)
{
    return n_port;
}

uint16_t node_info::GetID(void)
{
    return n_id;
}

char *node_info::GetAddress(void)
{
    char *ret = (char *) malloc(16 * sizeof(char));
    strncpy(ret, n_address, 16);
    return ret;
}

void PublicKeyInfo::init(const char addr[16], uint8_t router1, uint8_t router2, std::string pubKey1, std::string pubKey2)
{
	memcpy(p_address, addr, 16);
	p_verifyRouters[0] = router1;
	p_verifyRouters[1] = router2;
	
	p_pubK[0] = pubKey1;
	p_pubK[1] = pubKey2;
}

void Rip::Router_Init(uint16_t id, std::list<RipRte*> initialEntries, std::list<node_info> directlyConnectedNeighbors, std::string privKey, std::string pubKey, std::list<PublicKeyInfo> pks, bool test)
{
	if (LOG) std::cout << "Initializing RIP node " << id << ".\n";
	
	// Set the random seed so that it is different for each router. 
	srand(id);
	
	// Assign the public/private keys for this node
	privateKey = privKey;
	publicKey = pubKey;
	
	// Add the initial routing table entries to the class local routing table
	node_id = id;
	for (std::list<RipRte*>::iterator iter=initialEntries.begin(); iter != initialEntries.end(); ++iter)
	{
        // Create new routing table entries that will be going into the routing table
		RipRte *route = new RipRte();
		char ret[16] = {0};
		MaskAddress(ret, (*iter)->GetDestinationAddress(), (*iter)->GetSubnetMask());
		route->SetDestinationAddress(ret);
		route->SetSubnetMask((*iter)->GetSubnetMask());
		route->SetNextHop((*iter)->GetNextHop());
		route->SetRouteTag(1);
		route->SetRouteDistance((*iter)->GetRouteDistance());
		
		char *addr = route->GetDestinationAddress();
		uint32_t dist = route->GetRouteDistance();
		char sign[ENCRYPTED_LEN] = {0};
		construct_data_to_be_signed(addr, dist, sign);
		
		route->SetSignature(signMessage(privateKey, sign));
		
        if (dist != 0)
        {
            // This case allows us to simulate fake routes
            bzero(sign, ENCRYPTED_LEN);
            construct_data_to_be_signed(addr, dist-1, sign);
            route->SetSignature(signMessage(privateKey, sign));
        }
		else
        {
            char ver[ENCRYPTED_LEN] = "local";
            route->SetVerification(ver);
        }
        delete(addr);
        // Add the route into the routing table
		routingTable.push_front(route);
	}
	
	// Update the neighbor information that will be local to this node.
	assert(!directlyConnectedNeighbors.empty());
	for (std::list<node_info>::iterator iter = directlyConnectedNeighbors.begin(); iter != directlyConnectedNeighbors.end(); ++iter)
	{
		neighbors.push_front(*iter);
	}
	
    // Update the neighbor information that will be local to this node.
	assert(!pks.empty());
	for (std::list<PublicKeyInfo>::iterator iter = pks.begin(); iter != pks.end(); ++iter)
	{
		pub_keys.push_front(*iter);
	}
		
	// Initialize class variables
	delete_id = 0;
	response_id = 0;
	garbageCount = 0;
	
	minimumTriggeredDelay = 1.0;
    maximumTriggeredDelay = 5.0;
    periodicUpdateDelay = 30;
    routeDeletionDelay = 10;
	
	// Start the routing process by sending out request messages.
	if (!test) SendFullTableRequest();
	
	// Spawn a thread that will send periodic updates
	std::thread periodicResponse;
	periodicResponse = std::thread(&Rip::SpawnPeriodicResponse, this);
	periodicResponse.detach();
}

void Rip::Router_Destroy(void)
{
	if (LOG) std::cout << "Destroying RIP node " << this->node_id << ".\n";
	
	// Delete all of the routing table entries
	for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
	{
		delete(*iter);
	}
}

void Rip::PrintRoutingTables(void)
{
	printf("-----------------------------------------------------------\n");
	printf("RIP node %u routing table:\n", node_id);
	uint16_t i = 1;
	
    // Print out each valid route
	for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
	{
		if ((*iter)->GetRouteTag() == 1)
		{
			printf("Route %u:\n", i++);
			(*iter)->Print();
		}
	}
	
	printf("-----------------------------------------------------------\n");
}

void Rip::MaskAddress(char *ret, char *address, char *mask)
{
    // Do a basic mask of an address. We are only using 10.0.X.0/24 addresses so simplify the masking
	std::string testMask = "255.255.255.0";
	if (strcmp(mask, (char *) testMask.data()) != 0) return;
	
	char *addrTok = strtok(address, ".");
	
	int count = 0;
	for (int i = 0; i < 3; i++)
	{			
		strcpy(&ret[count], addrTok);
		int len = strlen(addrTok);
		ret[count + len] = '.';
		count = count + len + 1;
		
		addrTok = strtok(NULL, ".");
	}
	
	ret[count] = '0';
	ret[count+1] = '\0';
	
}

uint8_t Rip::DecodeRipPacket(void *pack, uint16_t senderPort)
{
	if (LOG) std::cout << "\nDecoding RIP packet: " << "\n";
	
    if (!this->VerifyRipPacket(pack))
    {
        // If this is not a proper RIP packet format, then drop the packet.
		if (LOG) std::cout << "Invalid RIP packet received.\n";
        return 0;
    }
    CommandHeader *CH = (CommandHeader *) pack;
    if (LOG) CH->Print();
    
    // The interface is used to identify the device in the network so we know who is 
    // sending messages and who to respond to in certain situations.
    uint32_t interface = CH->GetSender();
    
    if (LOG) std::cout << "Received a message from router: " << interface << "\n";
    
    // Get the type of command so that we know how to decode the packet
    CommandHeader::command comm = CH->GetCommand();
    if (comm == CommandHeader::REQUEST)
    {
        if (LOG) std::cout << "Got a request\n";
        ReceiveRequest(CH, senderPort, interface);
    }
    else if (comm == CommandHeader::RESPONSE)
    {
        if (LOG) std::cout << "Got a response\n";
        ReceiveResponse(CH, senderPort, interface);
    }
    else
    {
        std::cout << "Got an invalid RIP command packet\n";
    }
    
	return (uint8_t) comm;
}

bool Rip::VerifyRipPacket(void *pack)
{
    if (LOG) std::cout << "Verifying RIP packet: " << (char *) pack << "\n";
    
    // Check if the RIP packet is created properly. Drop packets that do not 
    // follow the proper format.
	CommandHeader *CH = (CommandHeader *) pack;
	
    if (CH->GetVersion () != 5)
    {
        // Verify the proper RIP version for this packet
        return false;
    }
    
    // Check the command type
    CommandHeader::command comm = CH->GetCommand();
    if (comm == CommandHeader::REQUEST)
	{
		return true;
	}
	else if (comm == CommandHeader::RESPONSE)
	{
		if (CH->GetListRtes().empty()) return false;
		return true;
	}
	else
	{
		return false;
	}
}
	
void Rip::ReceiveRequest(CommandHeader *pack, uint16_t senderPort, uint32_t interface)
{
	if (LOG) std::cout << "Received request from port " << senderPort << " on interface " << interface << ".\n";
	
	if (routingTable.empty()) return;
	
	std::list<RipRte> rteList = pack->GetListRtes();
	
	// If the routing table entry list is one, then we got a request for a full routing table
	if (rteList.size() == 1)
	{
        // Create a response packet with the current routing table entries
		CommandHeader response;
        response.SetCommand(CommandHeader::RESPONSE);
        response.SetSender(node_id);
		
        for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
        {
             response.AddRte(**iter);
        }
		
        if (LOG) {std::cout << "Sending a response to request to interface " << interface << " containing: \n"; response.Print();}
        SendTriggeredResponse(&response);
	}
	else
	{
		/* Not implemented because the RIP specification in RFC does not state that RIP nodes need 
         * to support individual entry requests and that they should only be implemented if a certain
         * application calls for it. Upon initialization, a router will simply request all routing table entries.
         */
	}
}
	
void Rip::ReceiveResponse(CommandHeader *pack, uint16_t senderPort, uint32_t interface)
{
	if (LOG) std::cout << "Received response from port " << senderPort << " on interface " << interface << ".\n";
	
	std::list<RipRte> rteList = pack->GetListRtes();
	bool changed = false;
	
    char nextHopAddress[16] = {0};
    for (std::list<node_info>::iterator iter = neighbors.begin(); iter != neighbors.end(); ++iter)
    {
        // Check the node id of each of the neighbors and check if the interface matches
        int neighbor_id = (*iter).GetID();
        if ((uint32_t) neighbor_id == interface)
        {
            char *incA = (*iter).GetAddress();
            char subMask[16] = "255.255.255.0";
            char ret[16] = {0};
            MaskAddress(ret, incA, subMask);
            memcpy(nextHopAddress, ret, 16);
        }
    }
    
    for (auto newIter = rteList.begin(); newIter != rteList.end(); newIter++)
	{        
		// Get the address and destination of the route.
		char *addr = (*newIter).GetDestinationAddress();
		uint32_t dist = (*newIter).GetRouteDistance();
		
		// Do a lookup to see if the new route exists currently in the routing table.
		RipRte *potential = BasicLookup(&(*newIter));
        
		// If the route is not already in the routing table
		if (potential == NULL)
		{
            // Get the next hop
            char *incHop = (*newIter).GetNextHop();
			// Get the verified string (ciphertext) from the incoming RTE.
			char *incVer = (*newIter).GetVerification();
			if (!VerifyRTE(addr, dist, incVer, incHop))
			{
				delete(addr);
				continue;		
			}
			
			// Sign the new RTE that might go into the routing table
			char sign[ENCRYPTED_LEN] = {0};
			construct_data_to_be_signed(addr, dist+1, sign);
			delete(addr);
			char *messageSignature = signMessage(privateKey, sign);
			
			// Add the new entry into the RT
			RipRte *addition = new RipRte();
			addition->SetDestinationAddress((*newIter).GetDestinationAddress());
			addition->SetSubnetMask((*newIter).GetSubnetMask());
			addition->SetNextHop(nextHopAddress);
			addition->SetRouteTag(1);
			addition->SetRouteDistance((*newIter).GetRouteDistance()+1);
			addition->SetSignature(messageSignature);
			addition->SetVerification((*newIter).GetSignature());
			
			routingTable.push_front(addition);
			if (LOG) printf("ADDED AN ENTRY\n");
			changed = true;
			
		}
		else
		{
			// We found the route, so now check if the route can be updated.
            auto incHop = (*newIter).GetNextHop();
			auto incDist = (*newIter).GetRouteDistance();
			
            // First check if the destination is unreachable
            if (incDist >= destinationUnreachable)
            {
                InvalidateRoute(potential);
				continue;
            }
            
			auto rtHop = potential->GetNextHop();
			auto rtDist = potential->GetRouteDistance();
			
			if (incDist+1 < rtDist)
			{
				// Get the verified string (ciphertext) from the incoming RTE.
				char * incVer = (*newIter).GetVerification();
				if (!VerifyRTE(addr, dist, incVer, incHop))
				{
					delete(addr);
					continue;		
				}
				
				// Sign the new RTE that might go into the routing table
				char sign[ENCRYPTED_LEN] = {0};
				construct_data_to_be_signed(addr, dist+1, sign);
				delete(addr);
				char *messageSignature = signMessage(privateKey, sign);
				
				if (strcmp(incHop, rtHop) != 0)
				{
					// If there is a new next hop on the path to the destination, add the new RTE
					
					// Delete the old route and add this new one
					InvalidateRoute(potential);
					
					// Add the new entry into the RT
					RipRte *addition = new RipRte();
					addition->SetDestinationAddress((*newIter).GetDestinationAddress());
					addition->SetSubnetMask((*newIter).GetSubnetMask());
					addition->SetNextHop(nextHopAddress);
					addition->SetRouteTag(1);
					addition->SetRouteDistance((*newIter).GetRouteDistance()+1);
					addition->SetSignature(messageSignature);
					addition->SetVerification((*newIter).GetSignature());
					
					// Add the new route
					routingTable.push_front(addition);
				}
				else
				{
					// The next hop is the same, so just resign with the new distance and then update the distance
					potential->SetRouteDistance((*newIter).GetRouteDistance()+1);
					potential->SetSignature(messageSignature);
				}
				if (LOG) printf("UPDATED AN ENTRY 2\n");
				changed = true;		
			}
			else
			{
				// If the distance is not less, then do not update the routing table
			}
			delete(incHop);
			delete(rtHop);
		}
	}
	
	if (changed)
	{
        // If the routing table changed, then we want to send a response to our neighbors
		if (LOG) printf("Added or updated an entry in the routing table.\n");
		CommandHeader response;
        response.SetCommand(CommandHeader::RESPONSE);
        response.SetSender(node_id);
		
        for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
        {
             response.AddRte(**iter);
        }
		
        if (LOG) {std::cout << "Updated RT... sending response to interface " << interface << " containing: "; response.Print();}
        SendTriggeredResponse(&response);
	}
}

bool Rip::VerifyRTE(char addr[16], uint16_t dist, char *verification, char hop[16])
{
	std::string local = "local";
	
	// Generate the string that is supposed to be in the incoming RTE verification
	char verify[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist-1, verify);
	std::string pT = verify;
	if (LOG) std::cout << pT << "\n"; // Print the data to be encrypted
	
	// First check if the address is local
	if (strcmp(verification, local.data()) != 0)
	{
		if (LOG) printf("---------------------------------------------------------\nSTARTING VERIFICATION PROCESS: %s\n", verification);
		bool legit = false, legit2 = false;
		for (std::list<PublicKeyInfo>::iterator iter = pub_keys.begin(); iter != pub_keys.end(); ++iter)
		{
			if (strcmp(hop, (*iter).p_address) != 0) continue;
			
			// Found the address, try the two keys that could have shared this info
			std::string pk0 = (*iter).p_pubK[0];
			std::string pk1 = (*iter).p_pubK[1];
			// Verify with the created string and ciphertext
			bool legit = verifySignature(pk0, pT, verification);
			if (!legit)
			{
				bool legit2 = verifySignature(pk1, pT, verification);
				if (!legit2)
				{					
					// Verification didn't work, so simply continue to the next element in the list
					break;
				}
			}
			if (LOG) printf("VERIFICATION WORKED!\n-----------------------------------------------------------\n");
			legit = true; legit2 = true;
			break;
		}
		// If we could not verify then simply continue to the next entry
		if (legit == false && legit2 == false)
		{
			if (LOG) printf("\nVERIFICATION FAILED!\n-------------------------------------------------------\n");
			return false;
		}
	}
	return true;
}

void Rip::SendFullTableRequest(void)
{
	if (LOG) std::cout << "Sending request packet.\n";
	
	// Sleep for one second before sending data
	std::this_thread::sleep_for (std::chrono::seconds((int) 1));
	
	// Create request packet
	CommandHeader request;
	request.SetCommand(CommandHeader::REQUEST);
    request.SetSender(node_id);
    
	RipRte requestRTE;
	char addr[16] = "255.255.255.255";
	char mask[16] = "0.0.0.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	requestRTE.SetDestinationAddress(addr);
	requestRTE.SetSubnetMask(mask);
	requestRTE.SetNextHop(hop);
	requestRTE.SetRouteTag(tag);
	requestRTE.SetRouteDistance(destinationUnreachable);
	request.AddRte(requestRTE);
	
	// Serialize the request packet
    char ret[(RTE_LEN * 1) + 13] = {0};
    uint32_t serializedSize = Serialize_CommandHeader(ret, request);
	
    for (std::list<node_info>::iterator iter = neighbors.begin(); iter != neighbors.end(); ++iter)
	{
        struct sockaddr_in routerAddr;
        socklen_t len = sizeof(routerAddr);
	
        // Get the port. The IP address is always loop back, but this could 
        // easily be changed if we wanted to run on a different system.
        int port = (*iter).GetPort();
		if (LOG) printf("Send request to port %d\n", port);
        std::string ip = "127.0.0.1";
        char *ip_addr = (char *) ip.data();
	
        if (LOG) printf("Sending a request message to router on port %d\n", port);
        
        // Setup a connection to the neighbor routers
        int reqSock = Setup_Connection(&routerAddr, ip_addr, port);
        
        // Send the serialied command header to the server
        write_to_socket(reqSock, (char *) ret, serializedSize, (sockaddr *)  &routerAddr, len);
        
        // Destroy the socket
        Destroy_Socket(reqSock);
	}
    
}

void Rip::SendResponseMessage(CommandHeader *CH)
{
    if (LOG) { std::cout << "Sending response packet with Header: \n"; CH->Print(); }
	
	// Serialize the response packet
    char ret[(RTE_LEN * MAX_RTES_PER_HEADER) + 5] = {0};
	uint32_t serializedSize = Serialize_CommandHeader(ret, *CH);
	
    for (std::list<node_info>::iterator iter = neighbors.begin(); iter != neighbors.end(); ++iter)
	{
        struct sockaddr_in routerAddr;
        socklen_t len = sizeof(routerAddr);
	
        // Get the port. The IP address is always loop back, but this could 
        // easily be changed if we wanted to run on a different system.
        int port = (*iter).GetPort();
        std::string ip = "127.0.0.1";
        char *ip_addr = (char *) ip.data();
	
        // Setup a connection to the neighbor routers
        int respSock = Setup_Connection(&routerAddr, ip_addr, port);
        
        // Send the serialied command header to the server
        write_to_socket(respSock, (char *) ret, serializedSize, (sockaddr *)  &routerAddr, len);
        
        // Destroy the socket
        Destroy_Socket(respSock);
	}
    
    
	response_lock.lock();
	response_id = 0;
	response_lock.unlock();
	//return NULL;
}

void Rip::SpawnPeriodicResponse(void)
{
    // Spawn periodic response messages
	for(int i = 0; i < 10; i++)
	{
		printf("Set up sending periodic response\n");
		SendPeriodicResponse();
	}
}

void Rip::SendPeriodicResponse(void)
{
	if (LOG) std::cout << "Setting up sending periodic update.\n";
	
    // Want to send a periodic response every 30 seconds
	std::this_thread::sleep_for (std::chrono::seconds((int) periodicUpdateDelay));
	
    // Create the response message to send to the neighbors
	CommandHeader response;
	response.SetCommand(CommandHeader::RESPONSE);
    response.SetSender(node_id);
	
	for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
	{
		 response.AddRte(**iter);
	}

	SendResponseMessage(&response);
	
}

void Rip::SendTriggeredResponse(CommandHeader *CH)
{
	if (LOG) std::cout << "Setting up sending triggered update.\n";
	
    // RIP's RFC states that responses should be sent out randomly between 1 and 5 seconds
	auto sleepTime = minimumTriggeredDelay + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(maximumTriggeredDelay-minimumTriggeredDelay)));
	float timeToSleep = sleepTime * 1000;
    printf("Sleeping for %f milliseconds\n",  timeToSleep);
	std::this_thread::sleep_for (std::chrono::milliseconds((int) timeToSleep));
	
	SendResponseMessage(CH);
}

RipRte* Rip::BasicLookup(RipRte *entry)
{
	if (LOG) {std::cout << "Basic lookup of  entry in routing table.\n"; /*entry->Print();*/}
	
    // Search the routing table by matching addresses and masks
	for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
	{
		if ((*iter)->GetRouteTag() == 0) continue;
		
		auto addr = (*iter)->GetDestinationAddress();
		auto mask = (*iter)->GetSubnetMask();
		
		if (strcmp(addr, entry->GetDestinationAddress()) != 0)
		{
			delete(addr);
			delete(mask);
			continue;
		}
		
		if (strcmp(mask, entry->GetSubnetMask()) != 0)
		{
			delete(addr);
			delete(mask);
			continue;
		}
		
		delete(addr);
		delete(mask);
		return (*iter);
	}
	
	return NULL;
}

RipRte* Rip::Lookup(RipRte *entry)
{
	if (LOG) {std::cout << "Looking up entry in routing table: "; entry->Print();}
	
    // Search the routing table by looking for the exact entry
	for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
	{
		if ((*iter)->GetRouteTag() == 0) continue;
		
		auto addr = (*iter)->GetDestinationAddress();
		auto mask = (*iter)->GetSubnetMask();
		auto hop = (*iter)->GetNextHop();
		auto dist = (*iter)->GetRouteDistance();
		
		if (strcmp(addr, entry->GetDestinationAddress()) != 0)
		{
			delete(addr);
			delete(mask);
			delete(hop);
			continue;
		}
		
		if (strcmp(mask, entry->GetSubnetMask()) != 0)
		{
			delete(addr);
			delete(mask);
			delete(hop);
			continue;
		}
		
		if (strcmp(hop, entry->GetNextHop()) != 0)
		{
			delete(addr);
			delete(mask);
			delete(hop);
			continue;
		}
		
		if (dist != entry->GetRouteDistance())
		{
			delete(addr);
			delete(mask);
			delete(hop);
			continue;
		}
		delete(addr);
		delete(mask);
		delete(hop);
		return (*iter);
	}
	
	return NULL;
}

bool Rip::InvalidateRoute(RipRte *entry)
{
	if (LOG) {std::cout << "Invalidating RIP route: "; entry->Print();}
	RipRte *invalidator = Lookup(entry);

	// Couldn't find the entry to invalidate
	if (invalidator == NULL)
	{
		return false;
	}
	
	// Invalidate entry and then delete the route
	if (invalidator->GetRouteTag() == 1)
	{
		invalidator->SetRouteTag(0);
		DeleteRoute(invalidator);
	}
	return true;
}

bool Rip::DeleteRoute(RipRte *route)
{
	if (LOG) {std::cout << "Deleting RIP route: "; route->Print();}
	
    // Search the routing table and then delete the entry
	for (auto iter = routingTable.begin(); iter != routingTable.end(); ++iter)
	{
		if (*iter == route)
         {
			if (LOG) std::cout << "Deleted a route\n";
           delete route;
           routingTable.erase (iter);
           return true;
         }
	}
	return false;
}