#ifndef CRYPTO_LIB
#define CRYPTO_LIB

#include <iostream>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <assert.h>
#include <cstring>


#define ENCRYPTED_LEN (349)

// Do a simple serialization of the data to be signed
bool construct_data_to_be_signed(char address[16], uint32_t distance, char ret[ENCRYPTED_LEN]);

// Deconstruct data that was previously serialzed together to be signed
void deconstruct_signed_data(char signedData[ENCRYPTED_LEN], char address[16], uint32_t *distance);

// Create an OpenSSL RSA private key
RSA* createPrivateRSA(std::string key);

// Create an OpenSSl RSA public key
RSA* createPublicRSA(std::string key);

// Sign a message using an RSA private key
bool RSASign( RSA* rsa,
              const unsigned char* Msg,
              size_t MsgLen,
              unsigned char** EncMsg,
              size_t* MsgLenEnc);

// Verify a message using an RSA public key
bool RSAVerifySignature( RSA* rsa,
                         unsigned char* MsgHash,
                         size_t MsgHashLen,
                         const char* Msg,
                         size_t MsgLen,
                         bool* Authentic);

// Encode the signed message so it can be used outside of the program
void Base64Encode( const unsigned char* buffer, size_t length, char** base64Text);

// Calculate the length of the encoded message
size_t calcDecodeLength(const char* b64input);

// Decode an encoded RSA signed message
void Base64Decode(const char* b64message, unsigned char** buffer, size_t* length);

// Sign message driver function
char* signMessage(std::string privateKey, std::string plainText);

// Verify message driver function
bool verifySignature(std::string publicKey, std::string plainText, char* signatureBase64);


#endif // CRYPTO_LIB