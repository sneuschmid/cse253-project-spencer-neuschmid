# Compiler
GCC = g++

# Compile options and linking
OPENSSL = -I /usr/include/openssl/ -L /usr/bin/openssl -lcrypto -lssl
OPTIMIZE = -O0
THREAD = -pthread -lpthread
OPTIONS = -march=native -Wall -lm 
CEREAL = -I $(PWD)
GDB = -g
LINKS = $(OPTIMIZE) $(THREAD) $(OPTIONS) $(CEREAL) $(GDB) $(OPENSSL)

# Object and final output file
ROUTER = router.o
RIP = rip.o
RIP_HEAD = rip-header.o
CRYPTO = crypto_lib.o
SOCK = sockets_lib.o
TEST = cli_test.o
OBJS = $(RIP) $(RIP_HEAD) $(CRYPTO) $(SOCK)
PROG = router
TEST_PROG = test

# Compile the program
all: $(PROG) $(TEST_PROG)

$(PROG): $(OBJS) $(ROUTER) router_info.h
	$(GCC) $(OBJS) $(ROUTER) -o $(PROG) $(LINKS) 

$(TEST_PROG): $(OBJS) $(TEST)
	$(GCC) $(OBJS) $(TEST) -o $(TEST_PROG) $(LINKS)
	
$(TEST): cli_test.cc
	$(GCC) $(LINKS) -c cli_test.cc

$(ROUTER): router.cc $(RIP) $(CRYPTO)
	$(GCC) $(LINKS) -c router.cc

$(RIP): rip.cc rip.h $(RIP_HEAD)
	$(GCC) $(LINKS) -c rip.cc

$(RIP_HEAD): rip-header.cc rip-header.h
	$(GCC) $(LINKS) -c rip-header.cc

$(CRYPTO): crypto_lib.cc crypto_lib.h
	$(GCC) crypto_lib.cc -c $(LINKS)
	
$(SOCK): sockets_lib.cc sockets_lib.h
	$(GCC)  $(LINKS)  -c sockets_lib.cc

# Delete executable and binary files
clean:
	rm -f $(OBJS) $(PROG) $(ROUTER) $(TEST_PROG) $(TEST)

# Used to run the program
run: clean all
	./$(PROG) -i 1


