#include "rip-header.h"

// RIP_RTE

RipRte::RipRte (void) :
    tag (0), destAddr ("127.0.0.1"),  subnetMask ("0.0.0.0"), nextHop ("0.0.0.0"), distance (16), signedMetric ("\0"), verifyMetric("\0")
{}

void RipRte::Print ()
{
    std::cout << "Destination address: " << destAddr << " Mask: " << subnetMask << " Metric: " << distance;
    std::cout << " Tag: " << tag << " Next Hop: " << nextHop << "\nSigned Metric: " << signedMetric 
                  << "\nVerify metric: " << verifyMetric << "\n";
}

void RipRte::SetDestinationAddress (char *address)
{
    strncpy(destAddr, address, 16 *sizeof(char));
}

char * RipRte::GetDestinationAddress (void)
{
    char *ret = (char *) malloc(16 * sizeof(char));
    strncpy(ret, destAddr, 16);
    return ret;
}

void RipRte::SetSubnetMask (char *mask)
{
    strncpy(subnetMask, mask, 16 *sizeof(char));
}

char* RipRte::GetSubnetMask (void) 
{
    char *ret = (char *) malloc(16 * sizeof(char));
    strncpy(ret, subnetMask, 16);
    return ret;
}

void RipRte::SetRouteTag (uint16_t routeTag)
{
    tag = routeTag;
}

uint16_t RipRte::GetRouteTag (void)
{
    return tag;
}

void RipRte::SetRouteDistance (uint32_t routeDist)
{
    distance = routeDist;
}

uint32_t RipRte::GetRouteDistance (void)
{
    return distance;
}

void RipRte::SetNextHop (char *hop)
{
    strncpy(nextHop, hop, 16 *sizeof(char));
}

char * RipRte::GetNextHop (void)
{
    char *ret = (char *) malloc(16 * sizeof(char));
    strncpy(ret, nextHop, 16);
    return ret;
}

void RipRte::SetSignature (char *sign)
{
	bzero(signedMetric, ENCRYPTED_LEN);
    memcpy(signedMetric, sign, ENCRYPTED_LEN * sizeof(char));
}

char * RipRte::GetSignature (void)
{
    char *ret = (char *) malloc((ENCRYPTED_LEN+1) * sizeof(char));
	bzero(ret, (ENCRYPTED_LEN+1) * sizeof(char));
    memcpy(ret, signedMetric, ENCRYPTED_LEN * sizeof(char));
    return ret;
}

void RipRte::SetVerification (char *verify)
{
    strncpy(verifyMetric, verify, ENCRYPTED_LEN * sizeof(char));
}

char *RipRte::GetVerification (void)
{
    char *ret = (char *) malloc((ENCRYPTED_LEN+1) * sizeof(char));
    bzero(ret, (ENCRYPTED_LEN+1) * sizeof(char));
	memcpy(ret, verifyMetric, (ENCRYPTED_LEN+1) * sizeof(char));
    return ret;
}


// RIP_COMMAND_HEADER

CommandHeader::CommandHeader (void) :
    command_type (INVALID) 
{}

void CommandHeader::Print (void)
{
    std::cout << "Command: " << command_type << " RTE List: \n";
    for (std::list<RipRte>::iterator it=listRTE.begin(); it != listRTE.end(); ++it)
    {
        it->Print();
    }
}
   
void CommandHeader::SetCommand (command c)
{
    command_type = c;
}

CommandHeader::command CommandHeader::GetCommand (void)
{
    return command_type;
}

void CommandHeader::SetSender (uint32_t id)
{
    sendID = id;
}

uint32_t CommandHeader::GetSender (void)
{
    return sendID;
}

uint8_t CommandHeader::GetVersion (void)
{
    return (uint8_t) version;
}

void CommandHeader::AddRte (RipRte rte)
{
    listRTE.push_front(rte);
}

void CommandHeader::DeleteRtes (void)
{
    listRTE.clear();
}

uint16_t CommandHeader::GetNumRtes (void)
{
    return listRTE.size();
}

std::list<RipRte> CommandHeader::GetListRtes (void)
{
    return listRTE;
}
