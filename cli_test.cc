
#include "crypto_lib.h"
#include "sockets_lib.h"
#include "rip.h"
#include "router_info.h"

RipRte* Route1(void)
{
	char addr[16] = "10.0.0.1";
	char mask[16] = "255.255.255.0";
	char hop[16] = "10.0.0.2";
	uint16_t tag = 1;
	uint16_t dist = 5;
	
	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey1, s);
	
	char v[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist-1, v);
	char *verify = signMessage(privateKey1, v);
	
	return CreateTestRoute(addr, mask, hop, tag, dist, sign, verify);
}

void basic_client(void)
{
    printf("Starting test client program.\n");
	
	struct sockaddr_in serverAddr;
	socklen_t len = sizeof(serverAddr);
	
	int port = 2001;
	std::string ip = "127.0.0.1";
	char *ip_addr = (char *) ip.data();
	
	int sockfd = Setup_Connection(&serverAddr, ip_addr, port);
	
    CommandHeader CH;
    CH.SetCommand(CommandHeader::RESPONSE);
    RipRte rte;
    memcpy(&rte, Route1(), 776);
    CH.AddRte(rte);
    CH.Print();

    char temp[1024] = {0};
    uint64_t serializedSize = Serialize_CommandHeader(temp, CH);

	write_to_socket(sockfd, (char *) temp, serializedSize, (sockaddr *)  &serverAddr, len);
	
	Destroy_Socket(sockfd);
	printf("Finishing test client program.\n");
}

// Socket must be global to be able to enable to the signal handling
int test_sock;
// Signal to handle when Ctrl+C ends a program. Want to ensure all sockets have been closed.
void cc_interrupt(int signum)
{
	if (1) printf("\nCtrl C pressed on the router... closing socket.\n");
	// Check if the file descriptors have been closed, if not close them
	if (fcntl(test_sock, F_GETFL) != -1) Destroy_Socket(test_sock);
	exit(1);
}

void basic_router_driver(void)
{
    /* Test setup:
     * Initialize router 1 as fake router for testing.
     * Setup sockets
     * Read from socket, wait for router 5 to send a request message
     * Decode and send back a response
     * Wait for response again from router 5.
     * End program
     */
    
	// Generate the list of public keys
	std::list<PublicKeyInfo> pks = Distribute_Public_Keys();
	
    // Create a signal interrupt function to make sure that all variables are deallocated
	signal(SIGINT, cc_interrupt);
    
    // Pretend to be router 1 in the test topology
    Rip node;
    int router_id = 1;
    int port = 1500;
    
    bool work = Setup_Router(router_id, &node, pks, true);
	if (!work)
		return;
    
    int bytesRead = 0;
	
	// Create and bind a new socket on the designated port
	int test_sock = Initialize_Server_UDP_Socket(port);
    
    // Create another address to get the clients info
	struct sockaddr_in clientInfo;
	socklen_t clientLen = sizeof(clientInfo);
	bzero(&clientInfo, clientLen);
	
	// Set the max read size in bytes based on the number of RTE entries 
	uint64_t maxReadSize = (RTE_LEN * MAX_RTES_PER_HEADER) + 4;
	// Create a buffer to store messages from the sender
	char buffer[maxReadSize+1] = {0};
	
	// Forever loop to read data from the client
	for(;;)
	{
		// Clear the last read and read from the socket
		bzero(buffer, maxReadSize);
		bytesRead = read_from_socket(test_sock, buffer, maxReadSize, (struct sockaddr *) &clientInfo, clientLen);		
		// Check if the socket timed out or another error occured
		if (bytesRead < 0) 
		{
            perror("recvfrom failed\n"); // This occurs if the socket was corrupted or closed
		}
		printf("Received a message:\n");
        
        // Deserialize the received RIP command
		CommandHeader CH = Deserialize_CommandHeader(buffer, maxReadSize);
		
		// Get the port from the client
        uint16_t client_port = ntohs(clientInfo.sin_port);
		
		// Decode the RIP packet and take an action based on the packet
		if (CommandHeader:: RESPONSE == node.DecodeRipPacket((void *) &CH, client_port))
        {
			node.PrintRoutingTables();
            break;
        }
		node.PrintRoutingTables();
		
	}
	
	// Destroy the router
	node.Router_Destroy();
	// Close the socket
	Destroy_Socket(test_sock);
    
}

int main(int argc, char **argv)
{
	basic_router_driver();
	return 0;
}
