#include "sockets_lib.h"

int Initialize_Server_UDP_Socket(int port)
{
	int socketfd = 0;
	// Create a new server address
	struct sockaddr_in serverAddr;
	bzero(&serverAddr, sizeof(serverAddr));
	
	// Open a new socket
	socketfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (socketfd < 0)
	{
		perror("Can't open socket\n");
		return 0;
	}
	
	// Add server information, sets up the original port for the server. Address set for any client.
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	serverAddr.sin_port = htons(port);
	
	// Bind the socket 
	if (bind(socketfd, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0) 
	{
		perror("Binding error\n");
		close(socketfd);
		return 0;
	}
	
	return socketfd;
}

bool Destroy_Socket(int socketfd)
{
	// Close the open socket
	close(socketfd);
	return true;
}

int Setup_Connection(struct sockaddr_in *serverAddr, char *ip_addr, int port)
{
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	
	// Initialize the server address to all zeros
	bzero((char *) serverAddr, sizeof(serverAddr));
	
	// Set the address family of the server
	serverAddr->sin_family = AF_INET;
	// Set the port number
	serverAddr->sin_port = htons(port);
	// Specify the IP address by the user input
	if (inet_pton(AF_INET, ip_addr, &serverAddr->sin_addr) <= 0)
	{
		fprintf(stderr, "assign ip address error at %s:%d\n", ip_addr,port);
		exit(1);
	}
	return sockfd;
}


int write_to_socket(int sockfd, char *inputBuffer, int writeLength, struct sockaddr *sendingToAddress, socklen_t addressLen)
{
	// Write to the socket
	int bytesWritten = sendto(sockfd, inputBuffer, writeLength, 0, sendingToAddress, addressLen);
	if (bytesWritten < 0) 
	{
		perror("Unable to write to the socket\n");
		return -1;
	}
	return bytesWritten;
}

int read_from_socket(int sockfd, char *readBuffer, int readLength, struct sockaddr *receivingFromAddress, socklen_t addressLen)
{
	// Read from the socket
	int bytesRead = recvfrom(sockfd, readBuffer, readLength, 0, receivingFromAddress, &addressLen);
	//printf("Read: %s\n", readBuffer); // Used for debugging
	if (bytesRead < 0) 
	{
		perror("Error reading from socket\n");
		return -1;
	}
	return bytesRead;
}

int Serialize_CommandHeader(char *ret, CommandHeader CH)
{
    // Use an output stream to serialize into an output
    std::ostringstream os;
    
    // Use cereal to serialize the CommandHeader
    {
		cereal::BinaryOutputArchive ar(os); // Create an output archive
		
		ar(CH); // Write the data to the archive
		
	} // archive goes out of scope, ensuring all contents are flushed
    
    // Now create the serialized string to give back to the caller
    std::string data = os.str();
	
    // Look at the message for debugging purposes
	//std::cout << "Writing data to socket: \n" << data << "\nwith size: " << data.size() << "\n\n\n";
	
    // Get the size and recreate a char * from the string
    uint64_t serializedSize = data.size();
    //printf("Serilaized size: %lu\n", serializedSize); 
    for (uint64_t i = 0; i < serializedSize; i++)
	{
		if (data.data()[i] == 0) continue; // Skip nulls because they are not necessary
		
		ret[i] = data.data()[i];
		//printf("%c", ret[i]); // Used for debugging
	}
	ret[serializedSize] = 0;
    
    // Debug to check if the recreated char * matches the string
    //printf("\n\nChar * data to socket: \n%s\n with size: %lu\n", ret, serializedSize);
    
    return serializedSize;
}

CommandHeader Deserialize_CommandHeader(char *serializedData, uint64_t size)
{
    // Create a new command header that will be returned to a user
    CommandHeader CH;
    
    {
        // Recreate the string
        std::string str = ""; 
        for (uint64_t i = 0; i < size; i++) 
        {
            //printf("%c", serializedData[i]); // Used for debugging
            str = str + serializedData[i]; 
        }
    
        // Debug to check if the received string is what it is expected to be
        //std::cout << "\nString received:\n" << str << "\n\n";
        
        // Create input stream for deserialization
        std::istringstream is(str);

        cereal::BinaryInputArchive ar(is); // Create an input archive

        ar(CH); // Read the data from the archive
    }
 
    return CH;
}