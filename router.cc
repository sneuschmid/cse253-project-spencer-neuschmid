
// Used to handle Ctrl+C and making sure all sockets are closed.
#include <signal.h>
// Command line input
#include <getopt.h>

// User created libraries
#include "sockets_lib.h"
#include "rip.h"
#include "rip-header.h"
#include "crypto_lib.h"
#include "router_info.h"

// Create three differet test routes that can be used for any testing purpose. Not used in the router code.
RipRte* Route1(void)
{
	char addr[16] = "10.0.0.1";
	char mask[16] = "255.255.255.0";
	char hop[16] = "10.0.0.2";
	uint16_t tag = 1;
	uint16_t dist = 5;
	
	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey1, s);
	
	char v[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist-1, v);
	char *verify = signMessage(privateKey1, v);
	
	return CreateTestRoute(addr, mask, hop, tag, dist, sign, verify);
}

RipRte* Route2(void)
{	
	char addr[16] = "10.0.0.2";
	char mask[16] = "255.255.0.0";
	char hop[16] = "10.0.0.6";
	uint16_t tag = 1;
	uint16_t dist = 10;

	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey1, s);
	
	char v[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist-1, v);
	char *verify = signMessage(privateKey1, v);
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, verify);
}

RipRte* Route3(void)
{	
	char addr[16] = "10.0.5.127";
	char mask[16] = "255.123.123.0";
	char hop[16] = "10.0.0.254";
	uint16_t tag = 1;
	uint16_t dist = 3;
	
	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey1, s);
	
	char v[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist-1, v);
	char *verify = signMessage(privateKey1, v);
	
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, verify);
}

// Test reading and writing RIP routing table entries.
void rte_test(void)
{
    RipRte *newRoute = Route1();
    newRoute->Print();
    
    char * add = newRoute->GetDestinationAddress();
    std::cout << "Destination address " << add << "\n";
    char * mas = newRoute->GetSubnetMask();
    std::cout << "Mask " << mas << "\n";
    char * hop = newRoute->GetNextHop();
    std::cout << "Hop " << hop << "\n";
    uint16_t tag = newRoute->GetRouteTag();
    std::cout << "Tag " << tag << "\n";
    uint32_t dist = newRoute->GetRouteDistance();
    std::cout << "dist " << dist << "\n";
    char * sig = newRoute->GetSignature();
    std::cout << "Signature " << sig << "\n";
    char * ver = newRoute->GetVerification();
    std::cout << "Verify " << ver << "\n";
    
    free(add);
    free(mas);
    free(hop);
    free(sig);
    free(ver);
}

// Test giving a request message to a RIP router.
// Delete a route see how the routing table changes.
void request_test(void)
{
    // Generate the list of public keys
	std::list<PublicKeyInfo> pks = Distribute_Public_Keys();
	
	// Create test routes
	RipRte *newRoute = Route1();
	RipRte *newRoute2 = Route2();
	
	Rip node;
	Setup_Router(1, &node, pks, false);
	
	
	if (node.Lookup(Route3()) != NULL)
		printf("\nSUCCCESS\n\n");
	else
		printf("\nFAILURE\n\n");
	
	node.InvalidateRoute(newRoute2); // Delete a route in the routing table
	
	node.PrintRoutingTables();
	// Create a test request message
    CommandHeader CH;
    CH.SetCommand(CommandHeader::REQUEST);
    CH.AddRte(*newRoute);
	node.DecodeRipPacket((void *) &CH, 0);
    node.PrintRoutingTables();
	
	// Destroy the router.
	node.Router_Destroy();
}

// Test giving a response message to a RIP router.
void response_test(void)
{
	// Generate the list of public keys
	std::list<PublicKeyInfo> pks = Distribute_Public_Keys();
	
	// Create a test route
	RipRte *newRoute3 = Route3();
	
	// Setup a RIP router
    Rip node;
	Setup_Router(1, &node, pks, false);
    
	node.PrintRoutingTables();
	// Create a test RIP response message
    CommandHeader CH;
    CH.SetCommand(CommandHeader::RESPONSE);
    CH.AddRte(*newRoute3);
    node.DecodeRipPacket((void *) &CH, 0);

	node.PrintRoutingTables();

	// Close the router
	node.Router_Destroy();
}

// Test signing various things such as strings and RIP metrics. This uses OpenSSL RSA.
void signing_test()
{
	// Basic signing example
	std::string plainText = "My secret message.\n";
	char* signature = signMessage(privateKey6, plainText);
	bool authentic = verifySignature(publicKey6, "My secret message.\n", signature);
	if ( authentic ) {
		std::cout << "Authentic" << std::endl;
	} else {
		std::cout << "Not Authentic" << std::endl;
	}
	
	// Create the data that will be signed in the RIP router and make sure we can decode it
    std::string metric = "10.0.0.1 / 1";
	
	char addre[16]  = "10.0.0.1";
	uint32_t dista = 1;
	char ret[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addre, dista, ret);
	
	char *cpyRet = strdup(ret);
	uint32_t retDist;
	char retAddr[16] = {0};
	deconstruct_signed_data(cpyRet, retAddr, &retDist);
	free(cpyRet);
	
	printf("Dist: %u Addr: %s\n", retDist, retAddr);
	
	// Sign and verify the metrics that will be used in the RIP routers
	char *sig = signMessage(privateKey2, metric);
	printf("\n%s\n", sig);
	 authentic = verifySignature(publicKey2, ret, sig);
    if ( authentic ) {
      std::cout << "Authentic" << std::endl;
    } else {
      std::cout << "Not Authentic" << std::endl;
    }
}

// Test using the cereal header library to serilize/deserialize RIP packets
void serialization_test(void)
{
	std::ostringstream os; // Use an output stream

  {
    cereal::BinaryOutputArchive ar(os); // Create an output archive

	// Create test data to serialize
    TestClass m1;
	m1.x = 5;
	m1.y = 10;
	strcpy(m1.z, "Hello world\n");
	m1.command_type = 1;
	RipRte temp;
	memcpy(&temp, Route1(), 776);
	m1.listRTE.push_front(temp);
	memcpy(&temp, Route2(), 776);
	m1.listRTE.push_front(temp);
	
    ar(m1); // Write the data to the archive
	
	std::cout << "\nSTRING TO ARCHIVE: " << os.str() << " \n";
  } // archive goes out of scope, ensuring all contents are flushed

	// Get the string of the serialized test data
	std::string data = os.str();
	
	// Print out the data that would be shared over sockets
	std::cout << "Writing data to socket: " << data << "\nwith size: " << data.size() << "\n\n\n";
	std::cout << "\nWriting data to socket: " << data.c_str() << "\nwith size: " << data.size() << "\n\n\n";

	// Copy the serialized data
	char temp[1600] = {0};
	for (uint64_t i = 0; i < data.size(); i++)
	{
		if (data.data()[i] == 0) {printf("\nyes"); continue;}
		
		temp[i] = data.data()[i];
		printf("%c", data.data()[i]);
	}
	temp[data.size()] = 0;
	printf("\n\n");
	// Print out the serialized data for debugging
	for (uint64_t i = 0; i < data.size(); i++)
	{
		printf("%c", temp[i]);
	}
	printf("\n\n");
	
	// Recreate the serialized string
    std::string str2 = ""; 
    for (uint64_t i = 0; i < data.size(); i++) { 
        str2 = str2 + temp[i]; 
    } 

	// Print out the recreated serialized string
	std::cout << "Writing data to socket: " << str2 << "\nwith size: " << str2.size() << "\n\n\n";

	std::istringstream is(str2); // Use an input string for deserialization
	{
		cereal::BinaryInputArchive ar(is); // Create an input archive

		TestClass m1;
		ar(m1); // Read the data from the archive

		// Print all of the deserialized data to make sure that it deserialized properly
		printf("\n\n\nX: %d\tY: %d\tZ: %s\tC: %d\n", m1.x, m1.y, m1.z, m1.command_type);
		m1.listRTE.front().Print();
		m1.listRTE.back().Print();
	}
}

// Socket must be global to be able to enable to the signal handling
int sockfd;
Rip node;
// Signal to handle when Ctrl+C ends a program. Want to ensure all sockets have been closed.
void cc_interrupt(int signum)
{
	if (1) printf("\nCtrl C pressed on the router... closing socket.\n");
	// Check if the file descriptors have been closed, if not close them
	if (fcntl(sockfd, F_GETFL) != -1) Destroy_Socket(sockfd);
	node.Router_Destroy();
	exit(1);
}

// Create a basic server side sockets test to be used with the cli_test.cc program.
void sockets_test()
{
    // Create a signal interrupt function to make sure that all variables are deallocated
	signal(SIGINT, cc_interrupt);
    
	// Define relevant variables
	int port = 2001;
	int bytesRead = 0;
	sockfd = Initialize_Server_UDP_Socket(port);
	
	// Create another address to get the clients info
	struct sockaddr_in clientInfo;
	socklen_t clientLen = sizeof(clientInfo);
	bzero(&clientInfo, clientLen);
	
	// Create a buffer to store messages from the sender
	char buffer[1025] = {0};
	
	// Forever loop to read data from the client
	for(;;)
	{
		// Clear the last read and read from the socket
		bzero(buffer, 1024);
		bytesRead = read_from_socket(sockfd, buffer, 1024, (struct sockaddr *) &clientInfo, clientLen);		
		// Check if the socket timed out or another error occured
		if (bytesRead < 0) 
		{
			if (errno == EWOULDBLOCK) 
			{
				perror("Socket timed out in server main\n");
				continue;
			} 
			else
			{
				perror("recvfrom failed\n"); // This occurs if the socket was corrupted or closed
			}
		}
        // Print the received message for debugging purposes
		printf("Received a message from the sender:\n");
		for (int i = 0; i < 1024; i++)
		{
			printf("%c", buffer[i]);
		}
		printf("\n");
		
        // Deserialize the received RIP command
		CommandHeader CH = Deserialize_CommandHeader(buffer, 1024);
        printf("\n");
        CH.Print(); // For debugging purposes
	}
	
    // Close the socket
	Destroy_Socket(sockfd);
}

void run_router(uint16_t router_id, uint64_t port)
{
	printf("Running router id: %u on port %lu\n", router_id, port);
	
	// Generate the list of public keys
	std::list<PublicKeyInfo> pks = Distribute_Public_Keys();
	
	// Create a signal interrupt function to make sure that all variables are deallocated
	signal(SIGINT, cc_interrupt);
	
	/* Setup the intial routing table entries in the router. This function determines 
	 * the simulation parameters based on router ID. Modify in the router_info.h file.
	 */
	bool work = Setup_Router(router_id, &node, pks, false);
	if (!work)
		return;
	
	int bytesRead = 0;
	
	// Create and bind a new socket on the designated port
	sockfd = Initialize_Server_UDP_Socket(port);
	
	// Create another address to get the clients info
	struct sockaddr_in clientInfo;
	socklen_t clientLen = sizeof(clientInfo);
	bzero(&clientInfo, clientLen);
	
	// Set the max read size in bytes based on the number of RTE entries 
	uint64_t maxReadSize = (RTE_LEN * MAX_RTES_PER_HEADER) + 13; // Add 13 because of the command header
	// Create a buffer to store messages from the sender
	char buffer[maxReadSize+1] = {0};
	
	// Forever loop to read data from the other routers
	for(;;)
	{
		// Clear the last read and read from the socket
		bzero(buffer, maxReadSize);
		bytesRead = read_from_socket(sockfd, buffer, maxReadSize, (struct sockaddr *) &clientInfo, clientLen);		
		// Check if the socket timed out or another error occured
		if (bytesRead < 0) 
		{
			if (errno == EWOULDBLOCK) 
			{
				perror("Socket timed out in router main\n");
				continue;
			} 
			else
			{
				perror("socket receive failed\n"); // This occurs if the socket was corrupted or closed
			}
		}
		
        // Deserialize the received RIP command
		CommandHeader CH = Deserialize_CommandHeader(buffer, maxReadSize);
		
		// Get the port from the client
        uint16_t client_port = ntohs(clientInfo.sin_port);
		
		// Decode the RIP packet and take an action based on the packet
		node.DecodeRipPacket((void *) &CH, client_port);
		node.PrintRoutingTables();
	}
	
	// Destroy the router
	node.Router_Destroy();
	// Close the socket
	Destroy_Socket(sockfd);
}

int main(int argc, char **argv)
{
	// Default command line argument values
	uint64_t port;
	uint16_t router_id = 0;
	
	int opt;
	// Parse the command line arguments
	while((opt = getopt(argc, argv, "i:")) != -1)
	{
		switch(opt)
		{
			// For each value, store the option into the relevant variables
			case 'i':
			{
				router_id = atoi(optarg);
				break;
			}
		}
	}
	
	if (router_id == 0)
	{
		// Router ID must be set, if not then just return
		printf("No router ID set... exiting program\n");
		return 0;
	}
	else if (router_id == 1)
	{
		port = 1500;
	}
	else
	{
		// Each port on each socket will be a multiple of its ID
		port = router_id * 1000;
	}

	// Run the router code
	run_router(router_id, port);

	/* Unit tests */
    // signing_test();
	
	//serialization_test();
	
	//sockets_test(); // Requires cli_test.cc program
	
    //request_test();

	//response_test();
	
	
	return 0;
}