#ifndef ROUT_INFO
#define ROUT_INFO

#include "rip.h"

/*
 * This file is the simulation configuration file that changes how the simulation 
 * is set up. This file contains each of the public and private keys that will be 
 * used in each of the routers. It also contains path and neighbor information so
 * that routers can communicate properly and give each other relevant information.
 * Any number of things about the routers can be configured in this file, except 
 * for the actual operation of the RIP router itself.
 */

// Include each of the public/private key pairs required for each router.
// Keys are of various size to show that both work.
#if 1

std::string privateKey1 ="-----BEGIN RSA PRIVATE KEY-----\n"\
"MIIEowIBAAKCAQEAy8Dbv8prpJ/0kKhlGeJYozo2t60EG8L0561g13R29LvMR5hy\n"\
"vGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+vw1HocOAZtWK0z3r26uA8kQYOKX9\n"\
"Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQApfc9jB9nTzphOgM4JiEYvlV8FLhg9\n"\
"yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68i6T4nNq7NWC+UNVjQHxNQMQMzU6l\n"\
"WCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoVPpY72+eVthKzpMeyHkBn7ciumk5q\n"\
"gLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUywQIDAQABAoIBADhg1u1Mv1hAAlX8\n"\
"omz1Gn2f4AAW2aos2cM5UDCNw1SYmj+9SRIkaxjRsE/C4o9sw1oxrg1/z6kajV0e\n"\
"N/t008FdlVKHXAIYWF93JMoVvIpMmT8jft6AN/y3NMpivgt2inmmEJZYNioFJKZG\n"\
"X+/vKYvsVISZm2fw8NfnKvAQK55yu+GRWBZGOeS9K+LbYvOwcrjKhHz66m4bedKd\n"\
"gVAix6NE5iwmjNXktSQlJMCjbtdNXg/xo1/G4kG2p/MO1HLcKfe1N5FgBiXj3Qjl\n"\
"vgvjJZkh1as2KTgaPOBqZaP03738VnYg23ISyvfT/teArVGtxrmFP7939EvJFKpF\n"\
"1wTxuDkCgYEA7t0DR37zt+dEJy+5vm7zSmN97VenwQJFWMiulkHGa0yU3lLasxxu\n"\
"m0oUtndIjenIvSx6t3Y+agK2F3EPbb0AZ5wZ1p1IXs4vktgeQwSSBdqcM8LZFDvZ\n"\
"uPboQnJoRdIkd62XnP5ekIEIBAfOp8v2wFpSfE7nNH2u4CpAXNSF9HsCgYEA2l8D\n"\
"JrDE5m9Kkn+J4l+AdGfeBL1igPF3DnuPoV67BpgiaAgI4h25UJzXiDKKoa706S0D\n"\
"4XB74zOLX11MaGPMIdhlG+SgeQfNoC5lE4ZWXNyESJH1SVgRGT9nBC2vtL6bxCVV\n"\
"WBkTeC5D6c/QXcai6yw6OYyNNdp0uznKURe1xvMCgYBVYYcEjWqMuAvyferFGV+5\n"\
"nWqr5gM+yJMFM2bEqupD/HHSLoeiMm2O8KIKvwSeRYzNohKTdZ7FwgZYxr8fGMoG\n"\
"PxQ1VK9DxCvZL4tRpVaU5Rmknud9hg9DQG6xIbgIDR+f79sb8QjYWmcFGc1SyWOA\n"\
"SkjlykZ2yt4xnqi3BfiD9QKBgGqLgRYXmXp1QoVIBRaWUi55nzHg1XbkWZqPXvz1\n"\
"I3uMLv1jLjJlHk3euKqTPmC05HoApKwSHeA0/gOBmg404xyAYJTDcCidTg6hlF96\n"\
"ZBja3xApZuxqM62F6dV4FQqzFX0WWhWp5n301N33r0qR6FumMKJzmVJ1TA8tmzEF\n"\
"yINRAoGBAJqioYs8rK6eXzA8ywYLjqTLu/yQSLBn/4ta36K8DyCoLNlNxSuox+A5\n"\
"w6z2vEfRVQDq4Hm4vBzjdi3QfYLNkTiTqLcvgWZ+eX44ogXtdTDO7c+GeMKWz4XX\n"\
"uJSUVL5+CVjKLjZEJ6Qc2WZLl94xSwL71E41H4YciVnSCQxVc4Jw\n"\
"-----END RSA PRIVATE KEY-----\n\0";

std::string publicKey1 ="-----BEGIN PUBLIC KEY-----\n"\
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAy8Dbv8prpJ/0kKhlGeJY\n"\
"ozo2t60EG8L0561g13R29LvMR5hyvGZlGJpmn65+A4xHXInJYiPuKzrKUnApeLZ+\n"\
"vw1HocOAZtWK0z3r26uA8kQYOKX9Qt/DbCdvsF9wF8gRK0ptx9M6R13NvBxvVQAp\n"\
"fc9jB9nTzphOgM4JiEYvlV8FLhg9yZovMYd6Wwf3aoXK891VQxTr/kQYoq1Yp+68\n"\
"i6T4nNq7NWC+UNVjQHxNQMQMzU6lWCX8zyg3yH88OAQkUXIXKfQ+NkvYQ1cxaMoV\n"\
"PpY72+eVthKzpMeyHkBn7ciumk5qgLTEJAfWZpe4f4eFZj/Rc8Y8Jj2IS5kVPjUy\n"\
"wQIDAQAB\n"\
"-----END PUBLIC KEY-----\n";


std::string privateKey2 = "-----BEGIN RSA PRIVATE KEY-----\n"\
"MIICXQIBAAKBgQC2jNpwCDTIDd9samG0QZsRDsqP/IUvCT2h98gZRD8TzzvYSkQn\n"\
"aWZEjjtZ/lbtgroy2X82MCM3d9AvrxMaCUDmQUFpHTUcGH2wzwZQPeYPN4E0NuMy\n"\
"NE9Gk2VBPb5l0SJfntZ9Iivrgh8G1Ch/5K1epRyhAJ5c6PCbogvI6nmwYwIDAQAB\n"\
"AoGAWohkVKddNdHbejMQHYtj0gPZ5arAimE01bzC2fUEq1hdP5klT75v35QCT1FZ\n"\
"3xS0Lz+6XS9mZ73d26gh+wmXy0K6bYQOUHVawC+GcLojnbc11jalkUbH6vN+taKW\n"\
"Z+Tcr+IzN3PZO31Q6zMM5MtehovbutnrPHJo+qPWw1R2QtECQQDmXYFKj6/MWhCH\n"\
"wQCjn6o4MB2OkKtLEaGKCwFa4G2PihRadYkmUojnvVpbqmmR+Z9RVj4XQgJs5qGL\n"\
"HmnY6gCXAkEAyt05c0+lAqiBVmuKi5s5k23HbrdAYfopVXUVY/YpkYlCiHQ4uJAv\n"\
"z7u6leL8UB+/8EcQ56Sb6LTzSyshEkX8FQJAZWbjcp6tE5bRj/A1B9FQbEtBt2+W\n"\
"9wRbxxfxEL+ZeoWdT/RM2sGZ4ehHzfHFZqwqpPMBgdtclfMvSL2qegEHIQJBAIYL\n"\
"N3LiJX5kKVIZLSqiHqEz4JtfqsMSsU0uWDv84+qub7AKW5BYdb8lpFx155Jp6EG0\n"\
"M49cTUiuiDQbA3c3lyECQQCLk3nltdyBMMvEvFwRu9mtL0y1XjI0iK1lF8IhW2cz\n"\
"a91krumSV4k/ikNumt+cbrwl4Oa4+C8GV8R45wqp6Aam\n"\
"-----END RSA PRIVATE KEY-----\n\0";

std::string publicKey2 = "-----BEGIN PUBLIC KEY-----\n"\
"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC2jNpwCDTIDd9samG0QZsRDsqP\n"\
"/IUvCT2h98gZRD8TzzvYSkQnaWZEjjtZ/lbtgroy2X82MCM3d9AvrxMaCUDmQUFp\n"\
"HTUcGH2wzwZQPeYPN4E0NuMyNE9Gk2VBPb5l0SJfntZ9Iivrgh8G1Ch/5K1epRyh\n"\
"AJ5c6PCbogvI6nmwYwIDAQAB\n"\
"-----END PUBLIC KEY-----\n\0";


std::string privateKey3 = "-----BEGIN RSA PRIVATE KEY-----\n"\
"MIICXAIBAAKBgQCUsLbI7tIqE4X6l4pOyVbntW18vn6NFtN2s8agoz94cxGMZlDa\n"\
"iDbtJOpHcMIXWvDKG+wWmqDA1jWt/7LYFaUZEL6/3HQTBDFuvUoCYwL1rMhIcxaO\n"\
"5HZmk4NpjB4zsDznNXjk9IRtyjzrGo9NcZydWIpV40Q5+ei6tXwY/6XplQIDAQAB\n"\
"An8PGeN2hMrTeyggS0PAvLIW1lZIVRCFQ8w2QgvqRKotkYI2wC12BF+PtjAVEV6l\n"\
"AJdz72IK6alp7FeHVHDVgV1DBi9BzH2TyfKOiDUMMsEEp7fBG7rVSY3ptTtE5zjc\n"\
"FRn979seMCmrsaoqb/TDld6wjWCGGOMWP3GxJxoPODO5AkEA6PBpZGJWaunwo2c+\n"\
"eornlzSfaDpKsK5gaTNy/Hvs7uMjXPvdFV+warubHP5M+WdmiyMDjbbrx2F/a+q6\n"\
"eGNhMwJBAKNpG44/Q7GmjRJdDq9YYqveZdTc17nP5gtaCO1CJjfi4QFe877XXFJb\n"\
"Tvxdyege0C81V4K8uS1Mv0HAgr/DGhcCQQCin8VUmfrvJWglDF1oPDW3MLvhjmC6\n"\
"vOL+TzcF4pblqJn1vrj4xRdrTJLcSQYAURocJVPDymMNjZniA7wvdGJVAkEAh0KO\n"\
"S400VE0/Qi8C48MuUYMSGUL/0c8zZLwO3Yy+o83JX2AgNByBT65C0AQ2B0vSGPF3\n"\
"UUEzqjp3qU5XK7Gr2wJBAKFoY5cA4yWPT1C0eNE+9DxO/+EJcM/a74AhF58TEBNX\n"\
"wqCt4bBE+BHvnSe/9kbufZJdrrlMgUDnUFhMcvB829c=\n"\
"-----END RSA PRIVATE KEY-----\n\0";

std::string publicKey3 = "-----BEGIN PUBLIC KEY-----\n"\
"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCUsLbI7tIqE4X6l4pOyVbntW18\n"\
"vn6NFtN2s8agoz94cxGMZlDaiDbtJOpHcMIXWvDKG+wWmqDA1jWt/7LYFaUZEL6/\n"\
"3HQTBDFuvUoCYwL1rMhIcxaO5HZmk4NpjB4zsDznNXjk9IRtyjzrGo9NcZydWIpV\n"\
"40Q5+ei6tXwY/6XplQIDAQAB\n"\
"-----END PUBLIC KEY-----\n\0";

std::string privateKey4 = "-----BEGIN RSA PRIVATE KEY-----\n"\
"MIICXAIBAAKBgQCbwwjVdnewjQD1uF2sza9x7j6wK6lN2KrZtJPNT5JwRSnlqw1M\n"\
"2D3dNg1b8rNmIxFMxZpRdRRqwswG70NX6Ckh9hc1CvEFCHYtspXZ7RXqhhUyugjV\n"\
"MPWStDRoYxkV5t0j3cIhqI6jfHEG7yrn/fOBkE8sCfQyoZWyAsCBeXtnTQIDAQAB\n"\
"AoGAIEtrxXZuknhpvoifQ3jR63lOHhCvZaiKo+DA1EyvO//bE/dHJqufCAJw+qNz\n"\
"drXGnHhL6p+vwALrcnShLdTA0iqUwtAhG4OrdK54rxlx4vhZcfPHwtbjDopvMBk0\n"\
"YO9gKvXbkJiIwIhiZlofXdz9iNOUMRQLsU89Yfxa4PvCM7ECQQDcKNt/3pQtD0v4\n"\
"p8dIod1GFdr9I+KpD0FXjqn51UplqTXMpATDQiAb7LKWyNc+UyXa+8YqcUnqtu+K\n"\
"PF14ibl7AkEAtR5nUUWeDwsV/YCx6iSwcF8B4Qj+6lw686M1jIZMTPUJAf94YbGg\n"\
"hSmLMqueHkpptFOv32vmBX70BnqyAj2T1wJAYSGdhdK7aQ3gpLnREFhpeWxXC2vz\n"\
"qkbSu0Bz+YeCpUt8MucCOutHo0pQmHnPjO2+O9js2vjUdbtz/3cWfgG1VQJBAImR\n"\
"3WIi4eqFPh++R+UUFxxTXccDITT79x8mAb+KJpxxpnTpChQSal2a74r+Rey2EtIQ\n"\
"ms62UxIrmsjtGxaOlSMCQEgWbRx0E3BN4raD6h3szyXpkGGOwpA0bpdkV5pRlfwf\n"\
"hIHRVst8pWtCQLQwyOAvBGLx1EbuTrhJjA3iTe6S5ek=\n"\
"-----END RSA PRIVATE KEY-----\n\0";

std::string publicKey4 = "-----BEGIN PUBLIC KEY-----\n"\
"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbwwjVdnewjQD1uF2sza9x7j6w\n"\
"K6lN2KrZtJPNT5JwRSnlqw1M2D3dNg1b8rNmIxFMxZpRdRRqwswG70NX6Ckh9hc1\n"\
"CvEFCHYtspXZ7RXqhhUyugjVMPWStDRoYxkV5t0j3cIhqI6jfHEG7yrn/fOBkE8s\n"\
"CfQyoZWyAsCBeXtnTQIDAQAB\n"\
"-----END PUBLIC KEY-----\n\0";


std::string privateKey5 = "-----BEGIN RSA PRIVATE KEY-----\n"\
"MIICWwIBAAKBgGLu5O+r41Hsc9AORTKYpuK4UjKeFrIFXQVJyzOFaNZ2wW8Z17Do\n"\
"WP2QFXnyZsIqK6PTY0TyvmNehnZP5woJKfPKHNlD3NpDze810w65k5t9gQXlmNM7\n"\
"kp87NKXXxDO4fNbNGhFgphFEquN6CymWQejBydzzQv3zvhrR9QVxi19hAgMBAAEC\n"\
"gYAHQzhFK94dJnl9/X4w76nYnOVT1+0clm40tC0ZlCKVE9SxXjlKQFRAEPKyqAvJ\n"\
"FyVq+lqgNh2p18eRKd5xCyksJ90mU2uKxWu8QuTMKozAzC7gYoxuZ2y5cCZlsk2q\n"\
"icHMJxKb14T9Ie3QHRxb28boUQ8qpwK8BfkLh5f//Jz5kQJBALCYNoalHk+a7WBf\n"\
"+FKlspOMPvbyAhIkWXLXDN7pz4jszzO2drc7eOwh5n6OFdYzO+8ShzgyWwlYdiv2\n"\
"RjQ4ba0CQQCPaxWJ+299uUbDJEc7TN+ok6LVERUsdPwJ0wsSv2vuoHoQVEin/RrF\n"\
"djPLTqAPuiugq/aGrzRboJMbngfyZ4cFAkB0Y738PDOgD+JH1Ljb7+2kpZiSNar7\n"\
"vg1eTTJkcQGho0JlOOtBlOb76tP5sTffN2FXDoUkmru5ix5Hl0dp/475AkAYJgTD\n"\
"bgXQ2pCSrRodXw1N2kYRsEWrbas+kD2OE5M94+GJvFdbid+nHgcMlZmX7FYKQMpK\n"\
"1VdwkjIS9WILK+ltAkEAoIjYxcgGSln+uQJ1d1SN5rVnqt91oY2186O/OD/dAVOF\n"\
"oEFI4Eoq5jwqvsjHQFu881M9RYn1D1lbKVw912WkzQ==\n"\
"-----END RSA PRIVATE KEY-----\n\0";

std::string publicKey5 = "-----BEGIN PUBLIC KEY-----\n"\
"MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGLu5O+r41Hsc9AORTKYpuK4UjKe\n"\
"FrIFXQVJyzOFaNZ2wW8Z17DoWP2QFXnyZsIqK6PTY0TyvmNehnZP5woJKfPKHNlD\n"\
"3NpDze810w65k5t9gQXlmNM7kp87NKXXxDO4fNbNGhFgphFEquN6CymWQejBydzz\n"\
"Qv3zvhrR9QVxi19hAgMBAAE=\n"\
"-----END PUBLIC KEY-----\n\0";


std::string privateKey6 = "-----BEGIN RSA PRIVATE KEY-----\n"\
"MIIEpgIBAAKCAQEAyEL8GTDHI5ljC/fY2ndPXfDWqbAtwumErHlAZ/y/RqbIH41m\n"\
"7sVJfSPvGwmSOKh7xe17By9LsPwOoz/bjxvJp4wT8LJeM8ujRiCQZpDSDO47qrqZ\n"\
"5kdkXGz2V7Q7COQvpL/cNkwGOn7EalFn0510RZ7OXHAkKxyj69FGnOi1yHYYnCsk\n"\
"qXgA5ou9Y0bgTTwKNLycyjaYsf8mQx6EVOm63ZEid94OcRuCo0PJM04i6u/Xm/OQ\n"\
"a7/5hALPpEJNAypYEYB6Rf5Y2OmHoctR0V+N/r3DwzYMcuUm4ESNF02AFd/IqpKa\n"\
"Z6vpOB6omAeXVXqefx9kUyqN0zc9lYpKq4A8MQIDAQABAoIBAQCRC4siB6VEUHVf\n"\
"lry2xtFyPdyMfLE4SgAxF4Uhi/tSBbxOthDGgZgPjjZBQ/ZnGQjEu5NvCb9tybIk\n"\
"ZTz18u9kbd2HaPk31JdGVJGh5+RrPyVyK8MJ+c1j7/ugXEah0vv6/IdXwEqGlYhX\n"\
"ZOypZZJwVmwqoYJgw1aqlyHoxdfb/vsVpcez0IRmL9Fp+/EhlXt7jB2owK9muYIM\n"\
"hBhJ3sM+OlU74UyBDnuuX5trs9vG/aXP+KBFVa7i784FVUizu0MluH9vR19THRVz\n"\
"qEQ6vPj22ZzNB9Pk97jlX18rb1SV0xkAG01ADEw6rowBYnmT9d/0CSYpDEO4ltYx\n"\
"p88vbcD9AoGBAP1cG6ycW2Yb9oPUmVOvvZcOYb/U/Tznokb64qYW2IVP/qdUV+f5\n"\
"AiPqCOxqrcWtDUQkYmE7Xvhh25RET2Q/521gI8bCjRmmkqOdBm9SJ1LrIxcUyR4k\n"\
"Ptpnd8YDTAQJpNGZX11DL/eTZVG+7k/AlvoUTTfJ0ITHCjH7cF5o7z8LAoGBAMpZ\n"\
"OdYRahJcUp7nBFd9ORZVFB0dyj2/fvVk7M34ppsu042ZPo7/paClSiXe9rDgDpHR\n"\
"nCcgVvjKykZM/BJ2iaIh7QoFm9mc2nMquQJuIKBRrrTzehc7af9zZlzkXsP+FIoT\n"\
"EemGLeyRjAFqFmZbvX8YvxS2r1Bu2kr8XwFpNCczAoGBAPt6I1XvVKFDfCpaKV5x\n"\
"f4OiGNAp/ronSGGfwLwOzRd10VQRWbe856zyLa1MLfOZ0yMh4iyUm616pFLEgvEE\n"\
"pRdhZ+FvVlJxjfescqioKLQbBJdyYDX00BZEwRPgTeFsGRaaX5fYipH40YaUN0Md\n"\
"7D2JTSPkw34nei0mX+C101MFAoGBAL9uVFIaHYCLSTq9kZT3ifd/u8yj7z4lmYWs\n"\
"2n8t+uibD1dJoC6G2vm1BgX3/uhswDvh0toNNVqf92c009DfxevWLrupfL0OMHh9\n"\
"9ZO//ivNuvvD0ECEXA5o7XF6N9Ex4dqSmgKUh4PkWlDIPbLOneLM1ZFSF19/0BLa\n"\
"2UKrdcSTAoGBAOWF1m7iov5OLdZa37ZP5YIcrk/7jU1aM8Eown67Dh3MNQIpUCZ9\n"\
"f8cuwy0D9eN99os1tpRW3adymSZn2P0zS0hWOlThGiIwGXvcXiy532ww9rTGjbSu\n"\
"tT5b8jVLF1OoDFAC2odqszSJPAYkH0eP1SjE3gKNZ6e+kdtWmDKZxOMq\n"\
"-----END RSA PRIVATE KEY-----\n\0";

std::string publicKey6 = "-----BEGIN PUBLIC KEY-----\n"\
"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyEL8GTDHI5ljC/fY2ndP\n"\
"XfDWqbAtwumErHlAZ/y/RqbIH41m7sVJfSPvGwmSOKh7xe17By9LsPwOoz/bjxvJ\n"\
"p4wT8LJeM8ujRiCQZpDSDO47qrqZ5kdkXGz2V7Q7COQvpL/cNkwGOn7EalFn0510\n"\
"RZ7OXHAkKxyj69FGnOi1yHYYnCskqXgA5ou9Y0bgTTwKNLycyjaYsf8mQx6EVOm6\n"\
"3ZEid94OcRuCo0PJM04i6u/Xm/OQa7/5hALPpEJNAypYEYB6Rf5Y2OmHoctR0V+N\n"\
"/r3DwzYMcuUm4ESNF02AFd/IqpKaZ6vpOB6omAeXVXqefx9kUyqN0zc9lYpKq4A8\n"\
"MQIDAQAB\n"\
"-----END PUBLIC KEY-----\n\0";


#endif

// Create a list of public keys that can be used to verify each of the addresses in the 
// network. Each router maintians this list.
std::list<PublicKeyInfo> Distribute_Public_Keys(void)
{
	std::list<PublicKeyInfo> pk;
	
	std::string addr = "10.0.0.0";
	PublicKeyInfo pk0;
	pk0.init(addr.data(), 1, 2, publicKey1, publicKey2);
	pk.push_front(pk0);
	
	addr = "10.0.1.0";
	PublicKeyInfo pk1;
	pk1.init(addr.data(), 2, 3, publicKey2, publicKey3);
	pk.push_front(pk1);

	addr = "10.0.2.0";
	PublicKeyInfo pk2;
	pk2.init(addr.data(), 2, 4, publicKey2, publicKey4);
	pk.push_front(pk2);
	
	addr = "10.0.3.0";
	PublicKeyInfo pk3;
	pk3.init(addr.data(), 3, 4, publicKey3, publicKey4);
	pk.push_front(pk3);

	addr = "10.0.4.0";
	PublicKeyInfo pk4;
	pk4.init(addr.data(), 4, 6, publicKey4, publicKey6);
	pk.push_front(pk4);
	
	addr = "10.0.5.0";
	PublicKeyInfo pk5;
	pk5.init(addr.data(), 5, 6, publicKey5, publicKey6);
	pk.push_front(pk5);
	
	addr = "10.0.6.0";
	PublicKeyInfo pk6;
	pk6.init(addr.data(), 1, 5, publicKey1, publicKey5);
	pk.push_front(pk6);
	return pk;
}

// Used to create routes that each router will initially use in their routing table entries.
RipRte* CreateTestRoute(char addr[16], char mask[16], char hop[16], uint16_t tag, uint16_t dist, char sign[ENCRYPTED_LEN], char verify[ENCRYPTED_LEN])
{
	// Set route information
    RipRte *newRoute = new RipRte();
    newRoute->SetDestinationAddress(addr);
    newRoute->SetSubnetMask(mask);
    newRoute->SetNextHop(hop);
    newRoute->SetRouteTag(tag);
    newRoute->SetRouteDistance(dist);
    newRoute->SetSignature(sign);
    newRoute->SetVerification(verify);
    return newRoute;
}


/* The following route information is essentially the simulation destup. Here we define the 
 * directly connected routes for each of the routers. These values should change if a 
 * different simulation setup wants to be run.
 */
// Router 1 routes
RipRte* Route_1_2(void)
{	
	char addr[16] = "10.0.0.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey1, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_1_5(void)
{	
	char addr[16] = "10.0.6.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey1, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

// Router 5 routes
RipRte* Route_5_1(void)
{
	char addr[16] = "10.0.6.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey5, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_5_6(void)
{
	char addr[16] = "10.0.5.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey5, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_5_3_fake(void)
{
	char addr[16] = "10.0.3.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 1;

	char s[ENCRYPTED_LEN] = {0};
	construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey5, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

// Router 2 routes
RipRte* Route_2_1(void)
{	
	char addr[16] = "10.0.0.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey2, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_2_3(void)
{	
	char addr[16] = "10.0.1.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey2, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_2_4(void)
{	
	char addr[16] = "10.0.2.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey2, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

// Router 3 routes
RipRte* Route_3_2(void)
{	
	char addr[16] = "10.0.1.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey3, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_3_4(void)
{	
	char addr[16] = "10.0.3.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey3, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

// Router 4 routes
RipRte* Route_4_2(void)
{	
	char addr[16] = "10.0.2.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey4, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_4_3(void)
{	
	char addr[16] = "10.0.3.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey4, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_4_6(void)
{	
	char addr[16] = "10.0.4.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey4, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

// Router 6 routes 
RipRte* Route_6_4(void)
{	
	char addr[16] = "10.0.4.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey6, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}

RipRte* Route_6_5(void)
{	
	char addr[16] = "10.0.5.0";
	char mask[16] = "255.255.255.0";
	char hop[16] = "0.0.0.0";
	uint16_t tag = 1;
	uint16_t dist = 0;

	char s[ENCRYPTED_LEN] = {0};
    construct_data_to_be_signed(addr, dist, s);
	char *sign = signMessage(privateKey6, s);
	
	char v[ENCRYPTED_LEN] = {0};
    return CreateTestRoute(addr, mask, hop, tag, dist, sign, v);
}


/* This function defines all of the starting values and directly connected neighbors of each 
 * router. This is more of the simulation setup and code should be changed below to change
 * the simulation.
 */
bool Setup_Router(uint16_t id, Rip *node, std::list<PublicKeyInfo> pks, bool test)
{
	// Define the port and router information
	uint16_t r1_port = 1500; uint16_t r1_id = 1;
	uint16_t r2_port = 2000; uint16_t r2_id = 2;
	uint16_t r3_port = 3000; uint16_t r3_id = 3;
	uint16_t r4_port = 4000; uint16_t r4_id = 4;
	uint16_t r5_port = 5000; uint16_t r5_id = 5;
	uint16_t r6_port = 6000; uint16_t r6_id = 6;
	
	// Each case in this switch statement corresponds to the setup of a specific router ID
	switch (id) 
	{
		case 1:
			{
				// Create the list of neighbors
				char addr[16] = "10.0.6.2";
				node_info neighbor1;
				neighbor1.init(addr, (uint16_t) r5_port, r5_id);
				char addr2[16] = "10.0.0.2";
				node_info neighbor2;
				neighbor2.init(addr2, (uint16_t) r2_port, r2_id);
				std::list<node_info> neighbor_list;
				neighbor_list.push_front(neighbor1);
				neighbor_list.push_front(neighbor2);
			
				// Create the initial routing table entries
				RipRte *route1 = Route_1_5();
				RipRte *route2 = Route_1_2();
				std::list<RipRte *> rte_list;
				rte_list.push_front(route1);
				rte_list.push_front(route2);
				
				// Initialize the router
				node->Router_Init(id, rte_list, neighbor_list, privateKey1, publicKey1, pks, test);
			}
			break;
		case 2:
			{
				// Create the list of neighbors
				char addr[16] = "10.0.0.1";
				node_info neighbor1;
				neighbor1.init(addr, (uint16_t) r1_port, r1_id);
				char addr2[16] = "10.0.1.1";
				node_info neighbor2;
				neighbor2.init(addr2, (uint16_t) r3_port, r3_id);
				char addr3[16] = "10.0.2.1";
				node_info neighbor3;
				neighbor3.init(addr3, (uint16_t) r4_port, r4_id);
				std::list<node_info> neighbor_list;
				neighbor_list.push_front(neighbor1);
				neighbor_list.push_front(neighbor2);
				neighbor_list.push_front(neighbor3);
				
				// Create the initial routing table entries
				RipRte *route1 = Route_2_1();
				RipRte *route2 = Route_2_3();
				RipRte *route3 = Route_2_4();
				std::list<RipRte *> rte_list;
				rte_list.push_front(route1);
				rte_list.push_front(route2);
				rte_list.push_front(route3);
				
				// Initialize the router
				node->Router_Init(id, rte_list, neighbor_list, privateKey2, publicKey2, pks, test);
			}
			break;
		case 3:
			{
				// Create the list of neighbors
				char addr[16] = "10.0.1.2";
				node_info neighbor1;
				neighbor1.init(addr, (uint16_t) r2_port, r2_id);
				char addr2[16] = "10.0.3.2";
				node_info neighbor2;
				neighbor2.init(addr2, (uint16_t) r4_port, r4_id);
				std::list<node_info> neighbor_list;
				neighbor_list.push_front(neighbor1);
				neighbor_list.push_front(neighbor2);
				
				// Create the initial routing table entries
				RipRte *route1 = Route_3_2();
				RipRte *route2 = Route_3_4();
				std::list<RipRte *> rte_list;
				rte_list.push_front(route1);
				rte_list.push_front(route2);
				
				// Initialize the router
				node->Router_Init(id, rte_list, neighbor_list, privateKey3, publicKey3, pks, test);
			}
			break;
		case 4:
			{
				// Create the list of neighbors
				char addr[16] = "10.0.2.2";
				node_info neighbor1;
				neighbor1.init(addr, (uint16_t) r2_port, r2_id);
				char addr2[16] = "10.0.3.1";
				node_info neighbor2;
				neighbor2.init(addr2, (uint16_t) r3_port, r3_id);
				char addr3[16] = "10.0.4.1";
				node_info neighbor3;
				neighbor3.init(addr3, (uint16_t) r6_port, r6_id);
				std::list<node_info> neighbor_list;
				neighbor_list.push_front(neighbor1);
				neighbor_list.push_front(neighbor2);
				neighbor_list.push_front(neighbor3);
			
				// Create the initial routing table entries
				RipRte *route1 = Route_4_2();
				RipRte *route2 = Route_4_3();
				RipRte *route3 = Route_4_6();
				std::list<RipRte *> rte_list;
				rte_list.push_front(route1);
				rte_list.push_front(route2);
				rte_list.push_front(route3);
				
				// Initialize the router
				node->Router_Init(id, rte_list, neighbor_list, privateKey4, publicKey4, pks, test);
			}
			break;
		case 5:
			{
                // Create the list of neighbors
				char addr[16] = "10.0.0.1";
				node_info neighbor1;
				neighbor1.init(addr, (uint16_t) r1_port, r1_id);
				char addr2[16] = "10.0.5.1";
				node_info neighbor2;
				neighbor2.init(addr2, (uint16_t) r6_port, r6_id);
                std::list<node_info> neighbor_list;
				neighbor_list.push_front(neighbor1);
				neighbor_list.push_front(neighbor2);
			
				// Create the initial routing table entries
				RipRte *route1 = Route_5_1();
                RipRte *route2 = Route_5_6();
                RipRte *route3 = Route_5_3_fake();
				std::list<RipRte *> rte_list;
				rte_list.push_front(route1);
				rte_list.push_front(route2);
                rte_list.push_front(route3);
                
				// Initialize the router
				node->Router_Init(id, rte_list, neighbor_list, privateKey5, publicKey5, pks, test);
			}
			break;
		case 6:
			{
				// Create the list of neighbors
				char addr[16] = "10.0.4.2";
				node_info neighbor1;
				neighbor1.init(addr, (uint16_t) r4_port, r4_id);
				char addr2[16] = "10.0.5.2";
				node_info neighbor2;
				neighbor2.init(addr2, (uint16_t) r5_port, r5_id);
				std::list<node_info> neighbor_list;
				neighbor_list.push_front(neighbor1);
				neighbor_list.push_front(neighbor2);
			
				// Create the initial routing table entries
				RipRte *route1 = Route_6_4();
				RipRte *route2 = Route_6_5();
				std::list<RipRte *> rte_list;
				rte_list.push_front(route1);
				rte_list.push_front(route2);
				
				// Initialize the router
				node->Router_Init(id, rte_list, neighbor_list, privateKey6, publicKey6, pks, test);
			}
			break;
		default:
			{
				printf("Invalid router id given.\n");
			}
			return false;
	}
	return true;
}

#endif // ROUT_INFO