#ifndef RIP_HEADER
#define RIP_HEADER

#include <chrono>
#include <iostream>
#include <ctime>
#include <vector>
#include <cstdint>
#include <array>
#include <list>
#include <cstring>

#include "crypto_lib.h"

// This defines the total length of a RIP routing table entry
#define RTE_LEN (776)
// This essentially defines the max packet size and will be used as the max number of neighbors
#define MAX_RTES_PER_HEADER (8)

class RipRte
{
public:
   RipRte (void);

    /* Print the contents of the RipRte */
    void Print (void);
    
    /* Set the destination address of a RipRte */
    void SetDestinationAddress (char *address);

    /* Return the destination address of a RipRte */
    char *GetDestinationAddress (void) ;

    /* Set the subnet mask of the address */
    void SetSubnetMask (char *mask);
 
    /* Get the subnet mask associated with the address */
    char *GetSubnetMask (void);

    /* Set the tag associated with the RTE */
    void SetRouteTag (uint16_t routeTag);

    /* Return the tag associated with the RTE */
    uint16_t GetRouteTag (void);

    /* Set the distance to the address */
    void SetRouteDistance (uint32_t routeDist);

    /* Return the distance to the address */
    uint32_t GetRouteDistance (void) ;

    /* Set the next hop on the path to the destination */
    void SetNextHop (char *hop);

    /* Get the next hop on the path to the destination */
    char *GetNextHop (void);

    /* Set the signature of the RTE, which is the address and distance signed by the current router */
    void SetSignature (char *sign);

    /* Get the signature assocaited with this RTE */
    char *GetSignature (void);

    /* Set the verification, which is the signed message of the previous router along the path (next hop) */
    void SetVerification (char *verify);

    /* Get the verification associated with this RTE */
    char *GetVerification (void);

    /* Cereal template that allows a RTE to be serialized */
    template<class Archive>
    void serialize(Archive & archive)
    {
        archive(tag, destAddr, subnetMask, nextHop, distance, signedMetric, pad, verifyMetric, pad2);
    }
    
private:
    // Information stored in a RTE
    uint16_t tag; 
    char destAddr[16]; 
    char subnetMask[16]; 
    char nextHop[16]; 
    uint32_t distance;
    char signedMetric[ENCRYPTED_LEN];
    char pad[10] = {0};
    char verifyMetric[ENCRYPTED_LEN];
    char pad2[10] = {0};
 };

/* This class defines the communication messages used in RIP */
class CommandHeader
{
public: 
    CommandHeader(void);

    /* Message types of RIP */
    enum command {
       INVALID = 0x0,
       REQUEST = 0x1,
       RESPONSE = 0x2
    };
    
    /* Print the RIP message */
    void Print (void);

    /* Set the message type */
    void SetCommand (command c);

    /* Get the command type of the message */
    command GetCommand (void);
    
     /* Set the sender ID */
    void SetSender (uint32_t id);

    /* Get the sender ID */
    uint32_t GetSender (void);
    
     /* Get the version */
    uint8_t GetVersion (void);

    /* Add a new RTE to the message */
    void AddRte (RipRte rte);

    /* Delete the RTEs in the RIP message */
    void DeleteRtes (void);

    /* Return the number of RTEs stored in the message */
    uint16_t GetNumRtes (void);

    /* Get the full list of RTEs stored in the message */
    std::list<RipRte> GetListRtes (void);

    /* Cereal template that allows a RIP message to be serialized */
    template<class Archive>
    void serialize(Archive & archive)
    {
         archive(command_type, version, sendID, listRTE);
    }

private:
    /* A RIp message consists of a  message type and a list of RTEs */
    command command_type;
    uint8_t version = 5; // Set the version as 5 for this new RIP version
    uint32_t sendID;
    std::list<RipRte> listRTE;
};

#endif // RIP_HEADER
