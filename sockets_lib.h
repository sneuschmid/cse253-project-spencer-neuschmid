#ifndef SOCKETS_H
#define SOCKETS_H

// Common libraries
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <math.h>

// Libraries needed for serialization
#include <cereal/archives/binary.hpp>
#include <cereal/types/list.hpp>
#include <sstream>
#include <list>

// Libraries needed for sockets
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

// Custom user libraries
#include "rip-header.h"

/* Server side in the following functions simply means when a router is receiving data. 
 * Data will be recevied at a router, then decoded. The decoded data is operated upon, 
 * and then a response might be sent back. At this time other sockets will be opened to 
 * send data in a more traditional 'client' way.
 */

// Initialize new UDP socket for the server side. This includes opening a new socket and binding it.
int Initialize_Server_UDP_Socket(int port);

// Delete any data for a server side socket
bool Destroy_Socket(int socketfd);

// Setup a connection to talk to a router
int Setup_Connection(struct sockaddr_in *serverAddr, char *ip_addr, int port);

// Functions used to write and read data to a socket
int write_to_socket(int sockfd, char *inputBuffer, int writeLength, struct sockaddr *serverAddress, socklen_t addressLen);
int read_from_socket(int sockfd, char *readBuffer, int readLength, struct sockaddr *serverAddress, socklen_t addressLen);

// Create a serialized version of the data
// Returns the size of the serialized data and returns a serialized string that is returned as char *ret
// Caller's job to make sure that char *ret is large enough to contain the serialized data.
int Serialize_CommandHeader(char *ret, CommandHeader CH);

// Deserialize a serialized command header
CommandHeader Deserialize_CommandHeader(char *serializedData, uint64_t size);


// Test class used for testing serialization with sockets. This will be removed and 
// replaced by RIP Command Headers.
class TestClass
{
	public:
	int x;
	int y;
	char z[16];
	int command_type;
    std::list<RipRte> listRTE;
	
     // Seriaize function used to serialize this class
	template<class Archive>
	void serialize(Archive & archive)
	{
		archive( x, y, z, command_type, listRTE );
	}
	private:
};


#endif // SOCKETS_H

