#ifndef RIP_H
#define RIP_H

// Standard C libraries
#include <chrono>
#include <iostream>
#include <ctime>
#include <vector>
#include <cstdint>
#include <thread>
#include <atomic>
#include <mutex>
#include <array>
#include <list>
#include <assert.h>
#include <pthread.h>
#include <random>

// User created libraries
#include "rip-header.h"
#include "crypto_lib.h"
#include "sockets_lib.h"


/* Class used to hold the neighbor information of each RIP node */
class node_info
{
public:
	/* Initilize a new neighbor */
	void init(char addr[16], uint16_t port, uint16_t id);
	
	/* Get the port of this neighbor */
    uint16_t GetPort(void);
    
    /* Get the id of the neighbor */
    uint16_t GetID(void);
    
    /* Get the address of the node_info */
    char *GetAddress (void) ;

private:
    char n_address[16];
	uint16_t n_port;
	uint16_t n_id;	
};

/* Class used to store public key information within each router */
class PublicKeyInfo
{
public:
	/* Initialize public key information class */
	void init(const char addr[16], uint8_t router1, uint8_t router2, std::string pubKey1, std::string pubKey2);

	char p_address[16];
	uint8_t p_verifyRouters[2];
	std::string p_pubK[2];
private:
};

/* RIP class that runs a RIP router */
class Rip
{
public: 
	
	/* Initialize the starting conditions for a router */
	void Router_Init(uint16_t id, std::list<RipRte*> initialEntries, std::list<node_info> directlyConnectedNeighbors, std::string privKey, std::string pubKey, std::list<PublicKeyInfo> pks, bool test);
	
	/* Delete a router instance and release all memory */
	void Router_Destroy(void);
	
    /* Print the routing tables for this RIP router. Used for debugging and verifcation. */
	void PrintRoutingTables(void);
	
	/* Mask address, mask the address that is in a RTE */
	void MaskAddress(char *ret, char *address, char *mask);
	
    /* Decode a RIP packet and determine what to do with it. */
	uint8_t DecodeRipPacket(void *pack, uint16_t senderPort);
	
	 /* Lookup to see if an entry is in our routing table, if so return the RTE */
	RipRte *Lookup(RipRte *entry);
		
    /* Invalidate a route and spawn a thread to destroy the route after a delay.
     * Return true if the route could be found, false otherwise.
     */
    bool InvalidateRoute(RipRte *entry);
	
private:
	typedef std::list<RipRte *> RoutesList;

    /* Variables used for a RIP router's functionality */
    RoutesList routingTable;
	std::mutex routingTableLock;
	
	uint16_t node_id;
    std::list<node_info> neighbors;
	
	// List of public keys required for verification
	std::list<PublicKeyInfo> pub_keys;
	
	// Variables needed to support multithreading
	std::mutex response_lock;
	pthread_t delete_id;
	pthread_t response_id;
	std::vector<std::thread> garbageThreads;
	uint16_t garbageCount;
	
	// Relevant RIP variables that are set by the RFCs
	const uint16_t destinationUnreachable = 16;
	float minimumTriggeredDelay;
    float maximumTriggeredDelay;
    uint16_t periodicUpdateDelay;
    uint16_t routeDeletionDelay;
	
	// Private and public keys used for crypto
	std::string privateKey;
	std::string publicKey;
	
    /* Verify the packet contents match a RIP packet */
	bool VerifyRipPacket(void *pack);
	
    /* We received a request message so handle dealing with this request */
	void ReceiveRequest(CommandHeader *pack, uint16_t senderPort, uint32_t interface);
	
    /* We received a response message so handle dealing with this response */
	void ReceiveResponse(CommandHeader *pack, uint16_t senderPort, uint32_t interface);
	
	/* Verifies an incoming RTE */
	bool VerifyRTE(char addr[16], uint16_t dist, char *verification, char hop[16]);
	
	/* Send a request message for all neighboring routes */
	void SendFullTableRequest(void);
	
	/* Send a response message that updates the nighbors with our current routing table */
	void SendResponseMessage(CommandHeader *CH);
	
	/* Spawn periodic response threads */
	void SpawnPeriodicResponse(void);
	
	/* Periodic messages should be sent out every 30 seconds */
	void SendPeriodicResponse(void);
	
	/* Triggered updates should be sent out after a delay of 1-5 seconds */
	void SendTriggeredResponse(CommandHeader *CH);

	 /* Lookup function that only finds an entry containing the proper address and subnet mask */
	RipRte *BasicLookup(RipRte *entry);
	
    /* Delete a route from the routing table. Return true if the route can be found,
     * false otherwise.
     */
    bool DeleteRoute(RipRte *route);
};

#endif // RIP_H